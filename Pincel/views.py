from .models import Projeto, Fonte, Registros, Alteracoes
from django.http.multipartparser import MultiPartParser
from pandas.tests.io.excel.test_xlrd import xlrd
from django.shortcuts import get_object_or_404
from django.http.response import JsonResponse
from django.db import IntegrityError
from openpyxl import load_workbook
from Papiro.views import conectar, alteracao_toDF
from io import TextIOWrapper
import psycopg2, csv

def projeto(request):
    if request.method == 'POST':
        nome = request.POST.get('nome')
        descricao = request.POST.get('descricao')
        
        if request.user.is_authenticated:   
            usuario = request.user
        else:
            return JsonResponse({'erro': 'Erro ao localizar usuário.'}, status=500)
        
        projeto = Projeto(
            usuario = usuario,
            nome = nome,
            descricao = descricao,
        )
        
        try:
            projeto.save()
            return JsonResponse({'data': projeto.id}, status=200)
        except IntegrityError as e:
            return JsonResponse({'erro': 'Erro ao salvar o projeto no banco de dados.', 'detalhes': e}, status=500)
    elif request.method == 'GET':
        retorno = []
        
        if request.user.is_authenticated:   
            usuario = request.user
        else:
            return JsonResponse({'erro': 'Erro ao localizar usuário.'}, status=500)
        
        projetos = Projeto.objects.filter(usuario=usuario)
        
        for projeto in projetos:
            retorno.append([projeto.id, projeto.nome, projeto.descricao, projeto.data_criacao])
        
        return JsonResponse({'data': retorno}, status=200)
    elif request.method == 'PUT':
        parser = MultiPartParser(request.META, request, request.upload_handlers)
        dados = parser.parse()
        
        id_projeto = dados[0].get('id')
        nome = dados[0].get('nome')
        descricao = dados[0].get('descricao')
        
        projeto = get_object_or_404(Projeto, id=id_projeto)
        
        projeto.nome = nome
        projeto.descricao = descricao
        
        try:
            projeto.save()
            return JsonResponse({'data': projeto.id}, status=200)
        except IntegrityError as e:
            return JsonResponse({'erro': 'Erro ao salvar alterações no projeto.', 'detalhes': e}, status=500)
    else:
        return JsonResponse({'error': 'Método de requisição não permitido.'}, status=405)

def deleta_projeto(request,id):
    if request.method == 'DELETE':
        projeto = get_object_or_404(Projeto, id=id)
        
        try:
            projeto.delete()
            return JsonResponse({'data': projeto.id}, status=200)
        except IntegrityError as e:
            return JsonResponse({'erro': 'Erro ao deletar o projeto.', 'detalhes': e}, status=500)
    else:
        return JsonResponse({'error': 'Método de requisição não permitido.'}, status=405)

def fonte(request):
    if request.method == 'POST':
        projeto_id = request.POST.get('projeto_id')
        banco = request.POST.get('banco')
        
        if banco != None:
            tipo = int(request.POST.get('tipo'))
            tecnologia = int(request.POST.get('tecnologia'))
            servidor = request.POST.get('servidor')
            usuario = request.POST.get('usuario')
            senha = request.POST.get('senha')
            porta = request.POST.get('porta')
            fontes = []
            
            try:
                conexao = conectar(tecnologia, servidor, usuario, senha, porta, banco)
                projeto = get_object_or_404(Projeto, id=projeto_id)
                
                fonte  = Fonte(
                    projeto = projeto,
                    tipo = tipo,
                    tecnologia = tecnologia,
                    usuario = usuario,
                    senha = senha,
                    endereco = servidor,
                    porta = porta, 
                    banco = banco
                )
                    
                fonte.save()
                
                if tecnologia != 0:
                    conexao.close()
                
                fontes = Fonte.objects.filter(projeto=projeto_id).order_by('id')
                fontes  = list(fontes.values()) 
            except psycopg2.Error as e:
                return JsonResponse({'data': 'Erro ao conectar com a base de dados.'}, status=500)
            
            return JsonResponse({'data': fontes}, status=201)
        elif 'arquivo' in request.FILES:
            arquivo = request.FILES['arquivo']
            tecnologia = int(request.POST.get('tecnologia'))
            extensao = arquivo.name.split(".")
                        
            if (extensao[1] == "xls" or extensao[1] == "xlsx") and tecnologia == 1:
                if extensao[1] == "xls":
                    arquivo = xlrd.open_workbook(file_contents=arquivo.read(), encoding_override="UTF-8")
                    folhas = len(arquivo.sheet_names())
                    
                    try:
                        for index in range(folhas):
                            controle = 0
                            folha = arquivo.sheet_by_index(index)
                        
                            if folha.cell(0,0).value == "":
                                controle = 1
                            if folha.cell(1,0).value == "":
                                break;
                        
                            projeto = get_object_or_404(Projeto, id=projeto_id)
                        
                            fonte = Fonte(
                                projeto = projeto,
                                tipo = 1,
                                tecnologia = 1,
                                banco = extensao[0]
                            )
                        
                            fonte.save()
                        
                            primeira = True
                            colunas = []
                        
                            for linha in range(controle, folha.nrows):
                                for celula in range(folha.ncols):
                                    if primeira:
                                        colunas.append(folha.cell(linha,celula).value)
                                        
                                        if celula == folha.ncols-1:
                                            primeira = False 
                                    else:
                                        registro = Registros(
                                            fonte = fonte,
                                            chave = colunas[celula],
                                            valor = folha.cell(linha,celula).value
                                        )
                        
                                        registro.save()
                        
                        fontes = Fonte.objects.filter(projeto=projeto_id).order_by('id')
                        fontes  = list(fontes.values())
                    except psycopg2.Error as e:
                        return JsonResponse({'data': 'Erro ao carregar o arquivo de dados.'}, status=500)
            
                    return JsonResponse({'data': fontes}, status=201)
                else:
                    livro = load_workbook(arquivo)
                    folha = livro.active
                    colunas = []
                    
                    try:
                        projeto = get_object_or_404(Projeto, id=projeto_id)
                        
                        fonte = Fonte(
                            projeto = projeto,
                            tipo = 1,
                            tecnologia = 2,
                            banco = extensao[0]
                        )
                    
                        fonte.save()
                        primeira = True
                        
                        for row in folha.iter_rows(values_only=True):
                            for indice, cell in enumerate(row):
                                if cell is None and len(colunas) == 0:
                                    break
                                else:
                                    if primeira:
                                        if cell != None:
                                            colunas.append(cell)
                                        
                                        if indice == len(row) - 1 :
                                            primeira = False
                                    else:
                                        if cell != None:
                                            registro = Registros(
                                                fonte = fonte,
                                                chave = colunas[indice],
                                                valor = cell
                                            )
                                            
                                            registro.save()
                                    
                        fontes = Fonte.objects.filter(projeto=projeto_id).order_by('id')
                        fontes  = list(fontes.values())        
                    except psycopg2.Error as e:
                        return JsonResponse({'data': 'Erro ao carregar o arquivo de dados.'}, status=500)
                    
                    return JsonResponse({'data': fontes}, status=201)
            
            elif extensao[1] == "csv" and tecnologia == 0:
                projeto = get_object_or_404(Projeto, id=projeto_id)
                arquivo = TextIOWrapper(arquivo, encoding='utf-8')
                reader = csv.reader(arquivo)
                colunas =  next(reader)
                
                if len(colunas) == 1:
                    arquivo.seek(0)
                    reader = csv.reader(arquivo, delimiter=";")
                    colunas =  next(reader)
                
                fonte = Fonte(
                    projeto = projeto,
                    tipo = 1,
                    tecnologia = 0,
                    banco = extensao[0]
                )
                
                try:
                    fonte.save()
                    
                    for linha in reader:
                        for indice, coluna in enumerate(linha):
                            registro = Registros(
                                fonte = fonte,
                                chave = colunas[indice],
                                valor = coluna
                            )
                            
                            registro.save()
                    
                    fontes = Fonte.objects.filter(projeto=projeto_id).order_by('id')
                    fontes  = list(fontes.values())        
                except psycopg2.Error as e:
                    return JsonResponse({'data': 'Erro ao carregar o arquivo de dados.'}, status=500)
                    
                return JsonResponse({'data': fontes}, status=201)   
            else:    
                return JsonResponse({'error': 'Arquivo no formato inválido.'}, status=405)
        else: 
            projeto = get_object_or_404(Projeto, id=projeto_id)
            fontes = Fonte.objects.filter(projeto=projeto).order_by('id')
            retorno = []
            
            for fonte in fontes:
                retorno.append({'id': fonte.id, 'tipo': fonte.tipo, 'tecnologia': fonte.tecnologia, 'banco': fonte.banco,'data_criacao': fonte.data_criacao})         
        
            return JsonResponse({'data': retorno}, status=200)        
    elif request.method == 'PUT':
        parser = MultiPartParser(request.META, request, request.upload_handlers)
        dados = parser.parse()
          
        projeto_id = dados[0].get('projeto_id')
        id_fonte = dados[0].get('id')
        tipo = dados[0].get('tipo')
        tecnologia = dados[0].get('tecnologia')
        servidor = dados[0].get('servidor')
        usuario = dados[0].get('usuario')
        senha = dados[0].get('senha')
        porta = dados[0].get('porta')
        banco = dados[0].get('banco')
        
        fonte = get_object_or_404(Fonte, id=id_fonte)
        alteracoes = Alteracoes.objects.filter(fonte=fonte.id)
            
        for alteracao in alteracoes:
            alteracao.delete()
        
        fonte.tipo = tipo
        fonte.tecnologia = tecnologia
        fonte.endereco = servidor
        fonte.usuario = usuario
        fonte.senha = senha
        fonte.porta = porta
        fonte.banco = banco
        
        fonte.save()
        
        fontes = Fonte.objects.filter(projeto=projeto_id).order_by('id')
        fontes  = list(fontes.values()) 
    
                
        return JsonResponse({'data': fontes}, status=201)
    else:
        return JsonResponse({'error': 'Método de requisição não permitido.'}, status=405)

def altera_fonte(request, id):
    if request.method == 'GET':
        fonte = get_object_or_404(Fonte, id=id)
        retorno = {
            'id': fonte.id,
            'tipo': fonte.tipo,
            'tecnologia': fonte.tecnologia,
            'banco': fonte.banco,
            'servidor': fonte.endereco,
            'porta': fonte.porta,
            'usuario': fonte.usuario,
            'senha': fonte.senha
        }
        
        return JsonResponse({'data': retorno}, status=200)
    elif request.method == 'POST':
        if 'arquivo' in request.FILES:
            arquivo = request.FILES['arquivo']
            tecnologia = int(request.POST.get('tecnologia'))
            extensao = arquivo.name.split(".")
                        
            if (extensao[1] == "xls" or extensao[1] == "xlsx") and tecnologia == 1:
                if extensao[1] == "xls":
                    arquivo = xlrd.open_workbook(file_contents=arquivo.read(), encoding_override="UTF-8")
                    folhas = len(arquivo.sheet_names())
                    try:
                        for index in range(folhas):
                            controle = 0
                            folha = arquivo.sheet_by_index(index)
                            
                            if folha.cell(0,0).value == "":
                                controle = 1
                            if folha.cell(1,0).value == "":
                                break;
                            
                            fonte = get_object_or_404(Fonte, id=id)
                            
                            fonte.banco = extensao[0]
                            fonte.tecnologia = 1
                            fonte.save()
                            
                            registros = Registros.objects.filter(fonte=fonte)
                            
                            for registro in registros:
                                registro.delete()
                            
                            primeira = True
                            colunas = []
                            
                            for linha in range(controle, folha.nrows):
                                
                                for celula in range(folha.ncols):
                                    if primeira:
                                        colunas.append(folha.cell(linha,celula).value)
                                        
                                        if celula == folha.ncols-1:
                                            primeira = False 
                                    else:
                                        registro = Registros(
                                            fonte = fonte,
                                            chave = colunas[celula],
                                            valor = folha.cell(linha,celula).value
                                        )
                            
                                        registro.save()
                            
                        fontes = Fonte.objects.filter(projeto=fonte.projeto.id).order_by('id')
                        fontes  = list(fontes.values())
                    except psycopg2.Error as e:
                        return JsonResponse({'data': 'Erro ao conectar com a base de dados.'}, status=500)
                    return JsonResponse({'data': fontes}, status=201)
                else:
                    planilha = load_workbook(arquivo)
                    folha = planilha.active
                    primeira = True
                    colunas = []
                    
                    fonte = get_object_or_404(Fonte, id=id)
                    fonte.banco = extensao[0]
                    fonte.tecnologia = 2
                    try:
                        fonte.save()
                                
                        registros = Registros.objects.filter(fonte=fonte)
                                
                        for registro in registros:
                            registro.delete()
                        
                        for row in folha.iter_rows(values_only=True):
                            for indice, cell in enumerate(row):
                                if cell is None and len(colunas) == 0:
                                    break
                                else:
                                    if primeira:
                                        if cell != None:
                                            colunas.append(cell)
                                        
                                        if indice == len(row) - 1 :
                                            primeira = False
                                    else:
                                        if cell != None:
                                            registro = Registros(
                                                fonte = fonte,
                                                chave = colunas[indice],
                                                valor = cell
                                            )
                                            
                                            registro.save()
                                    
                        fontes = Fonte.objects.filter(projeto=fonte.projeto.id).order_by('id')
                        fontes  = list(fontes.values())
                    except psycopg2.Error as e:
                        return JsonResponse({'data': 'Erro ao conectar com a base de dados.'}, status=500)
                    return JsonResponse({'data': fontes}, status=201)
            elif extensao[1] == "csv" and tecnologia == 0:
                arquivo = TextIOWrapper(arquivo, encoding='utf-8')
                reader = csv.reader(arquivo)
                colunas =  next(reader)
                
                if len(colunas) == 1:
                    arquivo.seek(0)
                    reader = csv.reader(arquivo, delimiter=";")
                    colunas =  next(reader)
                
                fonte = get_object_or_404(Fonte, id=id)
                fonte.tipo = 1
                fonte.tecnologia = 0
                fonte.banco = extensao[0]
                
                try:
                    fonte.save()
                    
                    registros = Registros.objects.filter(fonte=fonte)
                                
                    for registro in registros:
                        registro.delete()
                    
                    for linha in reader:
                        for indice, coluna in enumerate(linha):
                            registro = Registros(
                                fonte = fonte,
                                chave = colunas[indice],
                                valor = coluna
                            )
                            
                            registro.save()
                    
                    fontes = Fonte.objects.filter(projeto=fonte.projeto.id).order_by('id')
                    fontes  = list(fontes.values())        
                except psycopg2.Error as e:
                    return JsonResponse({'data': 'Erro ao carregar o arquivo de dados.'}, status=500)
                    
                return JsonResponse({'data': fontes}, status=201)   
            else:    
                return JsonResponse({'error': 'Arquivo no formato inválido.'}, status=405)
        else: 
            banco = request.POST.get('banco')
            fonte = get_object_or_404(Fonte, id=id)
            alteracoes = Alteracoes.objects.filter(fonte=fonte.id)
            
            for alteracao in alteracoes:
                alteracao.delete()
            
            if banco:
                fonte.banco = banco
            
            fonte.save()
            
        
            return JsonResponse({'data': "Tudo Certo"}, status=201)       
    if request.method == 'DELETE':
        fonte = get_object_or_404(Fonte, id=id)
        retorno = fonte.id
        
        try:
            fonte.delete()
            return JsonResponse({'data': retorno}, status=200)
        except IntegrityError as e:
            return JsonResponse({'erro': 'Erro ao deletar o fonte de dados.', 'detalhes': e}, status=500)
    else:
        return JsonResponse({'error': 'Método de requisição não permitido.'}, status=405)

def alteracao(request):
    if request.method == 'POST':
        fonte_id = request.POST.get('fonte_id')
        selecionados = request.POST.get('selecionados')
        relacao = request.POST.get('relacao')
        
        fonte = get_object_or_404(Fonte, id=fonte_id)
        
        try:
            if selecionados:
                alteracao = Alteracoes(
                    fonte = fonte,
                    tipo = 0,
                    valor = selecionados
                )
                
                alteracao.save()
                
            if relacao:
                alteracao = Alteracoes(
                    fonte = fonte,
                    tipo = 1,
                    valor = relacao
                )
                
                alteracao.save()
        except psycopg2.Error as e:
            return JsonResponse({'data': 'Erro ao carregar o arquivo de dados.'}, status=500)
            
        return JsonResponse({'data': "Tudo Certo"}, status=201)
    else:
        return JsonResponse({'error': 'Método de requisição não permitido.'}, status=405)

def extracao(request):
    if request.method == 'POST':
        fonte_id = request.POST.get('fonte_id')
        
        if fonte_id:
            fonte_id = int(fonte_id)
        
        fonte = get_object_or_404(Fonte, id=fonte_id)
        
        if fonte.tipo == 0:# Se tipo for igual a banco de dados
            selecionou = Alteracoes.objects.filter(fonte=fonte.id)
            
            if selecionou.exists():
                colunas, registros = alteracao_toDF(fonte, selecionou)
                
                if fonte.tecnologia == 0:
                    primeira = True
                    colunas = []
                    retorno = []
                    
                    for registro in registros:
                        if primeira:
                            colunas = list(registro.keys())
                            retorno.append(colunas)
                            primeira = False
                        
                        retorno.append([registro[coluna] for coluna in colunas])
                else:                
                    retorno = [list(registro) for registro in registros]
                    retorno.insert(0, colunas)
                
                return JsonResponse({'selecionou': True, 'data': retorno}, status=200)
            else:
                return JsonResponse({'selecionou': False}, status=200)
        else: # Se tipo for igual a arquivo
            registros = Registros.objects.filter(fonte=fonte.id).order_by('id')
            retorno = registros_toDF(registros)

        return JsonResponse({'data': retorno}, status=200)
    else:
        return JsonResponse({'error': 'Método de requisição não permitido.'}, status=405)

def registros_toDF(registros):
    colunas = []
    linha = []
    retorno = []
    primeira = True
    
    for registro in registros:
        if registro.chave in colunas:
            if primeira:
                primeira = False
                retorno.append(colunas)
                retorno.append(linha)
                linha = []
            
            linha.append(registro.valor)
            
            if len(linha) == len(colunas):
                retorno.append(linha)
                linha = []                
        else:
            colunas.append(registro.chave)
            linha.append(registro.valor)
    
    return retorno