from django.apps import AppConfig


class PincelConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Pincel'
