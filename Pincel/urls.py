from django.urls import path
from . import views

urlpatterns = [
    path('projeto', views.projeto, name="projeto"),
    path('projeto/<int:id>/', views.deleta_projeto, name='projeto_com_id'),
    path('fonte', views.fonte, name="fonte"),
    path('fonte/<int:id>/', views.altera_fonte, name='fonte_com_id'),
    path('extracao', views.extracao, name='extracao'),
    path('alteracao', views.alteracao, name='alteracao'),
]