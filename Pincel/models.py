from django.contrib.auth.models import User
from django.db import models

class Projeto(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    nome = models.CharField(max_length=100, null=False)
    descricao = models.TextField()
    data_criacao = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.nome
    
class Fonte(models.Model):
    projeto = models.ForeignKey(Projeto, on_delete=models.CASCADE)
    tipo = models.IntegerField(null=False)
    tecnologia = models.IntegerField(null=False)
    banco = models.CharField(max_length=100, null=True)
    endereco = models.CharField(max_length=255, null=True)
    usuario = models.CharField(max_length=100, null=True)
    senha = models.CharField(max_length=100, null=True)
    porta = models.CharField(max_length=10, null=True)
    data_criacao = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.banco+"_"+self.projeto.nome

class Alteracoes(models.Model):
    fonte = models.ForeignKey(Fonte, on_delete=models.CASCADE)
    tipo = models.IntegerField(null=False)
    valor = models.CharField(max_length=255, null=False)
    data_criacao = models.DateTimeField(auto_now_add=True)
    
class Registros(models.Model):
    fonte = models.ForeignKey(Fonte, on_delete=models.CASCADE)
    chave = models.CharField(max_length=100, null=False)
    valor = models.CharField(max_length=255, null=False)