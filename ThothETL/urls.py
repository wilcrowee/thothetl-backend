from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static 
from django.urls.conf import include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('Autenticacao.urls')),
    path('papiro/', include('Papiro.urls')),
    path('pincel/', include('Pincel.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


