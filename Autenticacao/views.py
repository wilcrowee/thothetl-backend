from django.contrib.auth.models import User
from django.contrib.auth import  authenticate, login, logout
from django.http.response import JsonResponse
from django.middleware.csrf import get_token

def Usuario(request):
    if request.method == 'GET':
        if request.user.is_authenticated:
            usuario = []
            usuario.append(request.user.username)
            usuario.append(request.user.email)
            
            return JsonResponse({'data': True, 'user_id': request.user.id, 'usuario': usuario, 'csrftoken': get_token(request)}, status=200)
        else:
            return JsonResponse({'data': False, 'csrftoken': get_token(request)}, status=200) 
    else:
        return JsonResponse({'error': 'Método de requisição não permitido.'}, status=405)
        
def Login(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        senha = request.POST.get('password')
        
        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            user = None
        
        if user:
            user = authenticate(request, username=user.username, password=senha)
        
        if user is not None:
            login(request, user)
            return JsonResponse({'data': True}, status=200)
        else:
            return JsonResponse({'error': 'Credenciais inválidas.'}, status=400)
    else:
        return JsonResponse({'error': 'Método de requisição não permitido.'}, status=405)

def Logout(request):
    if request.method == 'POST':
        logout(request)
        return JsonResponse({'message': 'Logout realizado com sucesso.'}, status=200)
    else:
        return JsonResponse({'error': 'Método de requisição não permitido.'}, status=405)

def Registrar(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        senha = request.POST.get('password')
        usuario = request.POST.get('username')
        
        try:
            user_username = User.objects.get(username=usuario)
        except User.DoesNotExist:
            user_username = None
        
        try:
            user_email = User.objects.get(email=email)
        except User.DoesNotExist:
            user_email = None
        
        
        if user_email is not None:
            return JsonResponse({'error': 'Email já cadastrado.', 'campo': 'email'}, status=400)
        elif user_username is not None:
            return JsonResponse({'error': 'Usuário já cadastrado.', 'campo': 'usuario'}, status=400)
        else:
            novo_usuario = User.objects.create_user(username=usuario, email=email, password=senha)
        
        return JsonResponse({'data': True}, status=200)
    else:
        return JsonResponse({'error': 'Método de requisição não permitido.'}, status=405)
