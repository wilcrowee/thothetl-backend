from django.urls import path
from . import views

urlpatterns = [
    path('identificaNota', views.identifica_nota, name='identifica_nota'),
    path('normalizaNota', views.normaliza_nota, name='normaliza_nota'),
]
