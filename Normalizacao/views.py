from django.views.decorators.csrf import csrf_exempt
from django.http.response import JsonResponse
import json, re

@csrf_exempt
def identifica_nota(request):
    if request.method == 'POST':
        dataFrame = json.loads(request.POST.get('dataFrame'))
        dfAuxiliar = request.POST.get('dfAuxiliar')
        expressao_regular = re.compile(r'^(10|([0-9A-Fa-f]([.,]\d+)?))$')
        retorno = []
        
        retorno.append(identifica(dataFrame, expressao_regular))        
        
        if dfAuxiliar:
            dfAuxiliar = json.loads(dfAuxiliar)
            retorno.append(identifica(dfAuxiliar, expressao_regular))        
        
        return JsonResponse({'data': retorno}, status=200)        
    else:
        return JsonResponse({'error': 'Método de requisição não permitido.'}, status=405)

@csrf_exempt   
def normaliza_nota(request):
    if request.method == 'POST':
        dataFrame = json.loads(request.POST.get('dataFrame'))
        dfAuxiliar = request.POST.get('dfAuxiliar') 
        localizacao = json.loads(request.POST.get('notas'))
        metodos = json.loads(request.POST.get('metodos')) # 0 - Nao faz nada | 1 - Converte para Letra | 2 - Converte para Numero
        decimais = json.loads(request.POST.get('decimais')) # 0 - Inteiro | 1 - 1 casa | 2 - 2 casas
        retorno = []
        dicionario = {}
        dicDecimais = {}
        
        dicionario, dicDecimais = popula_dicionario(localizacao, metodos, decimais)
        dataFrame = aplica_metodo_nota(dicionario, dicDecimais, dataFrame)
        retorno.append(dataFrame)
        
        if dfAuxiliar:
            dfAuxiliar = json.loads(dfAuxiliar)
            notasAuxiliar = json.loads(request.POST.get('notasAuxiliar'))
            metAuxiliar = json.loads(request.POST.get('metAuxiliar'))
            decAuxiliar = json.loads(request.POST.get('decAuxiliar'))
            
            dicionario, dicDecimais = popula_dicionario(notasAuxiliar, metAuxiliar, decAuxiliar)
            dfAuxiliar = aplica_metodo_nota(dicionario, dicDecimais, dfAuxiliar)
            
            retorno.append(dfAuxiliar)
            
            if len(dataFrame) == 0 and len(dfAuxiliar) == 0:
                retorno = []
        else:
            if len(dataFrame) == 0:
                retorno = []        
                        
        return JsonResponse({'data': retorno}, status=200)          
    else:
        return JsonResponse({'error': 'Método de requisição não permitido.'}, status=405)

def identifica(dataFrame, expressao_regular):
    validacao = []
    retorno = []
    
    for linha in range(len(dataFrame)):
        for coluna in range(len(dataFrame[linha])):    
            if linha == 0:
                validacao.append(True)
            else:
                valor = dataFrame[linha][coluna]
                    
                if isinstance(valor, int) or isinstance(valor, float):
                    valor = str(valor)
                
                if expressao_regular.match(valor):
                    validacao[coluna] = validacao[coluna] and True
                else:
                    validacao[coluna] = validacao[coluna] and False

    for item in range(len(validacao)):
        if validacao[item]:
            retorno.append(dataFrame[0][item])
    
    return retorno

def popula_dicionario(localizacao, metodos, decimais):
    dicionario = {}
    dicDecimais = {}
    
    for item in range(len(localizacao)):
        if metodos[item] != '0':
            dicionario[localizacao[item]] = metodos[item]
        
    for item in decimais:
        dicDecimais[item[0]] = item[1]
        
    return dicionario, dicDecimais

def aplica_metodo_nota(dicionario, dicDecimais, dataFrame):
    conversao = {'A': 10, 'B': 8, 'C': 6, 'D': 4, 'E': 2, 'F': 0}  # Mapeamento das notas para números
        
    if len(dicionario) != 0:
        for linha in range(len(dataFrame)):
            if linha != 0:
                for coluna in range(len(dataFrame[linha])):
                    if dataFrame[0][coluna] in dicionario:
                        if dicionario[dataFrame[0][coluna]] == '1':
                            dataFrame[linha][coluna] = converte_nota(dataFrame[linha][coluna])
                        elif dicionario[dataFrame[0][coluna]] == '2':
                            aux =  dataFrame[linha][coluna]
                            
                            if isinstance(aux, int):
                                aux = str(aux)
                                
                            if aux.isalpha():
                                dataFrame[linha][coluna] = conversao[dataFrame[linha][coluna].upper()]
                            else:
                                decimal = dicDecimais[dataFrame[0][coluna]]
                                aux = float(aux)
                                
                                if decimal == '0':
                                    dataFrame[linha][coluna] = round(aux)
                                elif decimal == '1':
                                    dataFrame[linha][coluna] = round(aux,1)
                                elif decimal == '2':
                                    dataFrame[linha][coluna] = round(aux,2)
        
        return dataFrame
    else:
        return []

def converte_nota(nota):
    if isinstance(nota, str):
        if nota.isalpha():
            return nota.upper()
        else:
            nota = round(float(nota))
        
    if nota >=9:
        return 'A'
    elif nota >= 7:
        return 'B'
    elif nota >= 5:
        return 'C'
    elif nota >= 3:
        return 'D'
    elif nota >= 1:
        return 'E'
    else:
        return 'F'