from django.apps import AppConfig

class PapiroConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Papiro'
