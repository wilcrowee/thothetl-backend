from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404
from django.http.response import JsonResponse
from Pincel.models import Fonte
import psycopg2, mysql.connector, pymongo, json

@csrf_exempt
def enviar_dados(request):
    if request.method == 'POST':
        tipo = int(request.POST.get('tipo'))
        servidor = request.POST.get('servidor')
        usuario = request.POST.get('usuario')
        senha = request.POST.get('senha')
        porta = request.POST.get('porta')
        banco = request.POST.get('banco')
        tabelas = json.loads(request.POST.get('tabela')) 
        relacoes = request.POST.get('relacao')
        
        if relacoes:
            relacoes = eval(relacoes)
        
        con = conectar(tipo, servidor, banco, usuario, senha, porta)
        
        if tipo == 0:
            retorno = list(mongo_documentos(con, tabelas, relacoes))
            
            aux = []
            
            for linha in retorno:
                secundario = []
                
                for item in linha.values():
                    secundario.append(item)
                
                aux.append(secundario)
                
            retorno = aux
        else:
            query = monta_select(tabelas, relacoes)
            retorno = executa_query(con, query)
        
        colunas = []
        
        for tabela in tabelas:
            for coluna in tabelas[tabela]:
                verifica = verifica_colunas(coluna, tabelas) 
                
                if verifica:
                    for item in verifica:
                        indice = tabelas[item].index(coluna)
                        tabelas[item][indice] = coluna+"_"+item
                    
                    colunas.append(coluna+"_"+tabela)
                else:
                    colunas.append(coluna)
        
        retorno = [colunas] + retorno
        
        return JsonResponse({'data': retorno}, status=200)
    else:
        return JsonResponse({'error': 'Método de requisição não permitido.'}, status=405)

@csrf_exempt
def juncao_vertical(request):
    if request.method == 'POST':
        principal = json.loads(request.POST.get('principal'))
        auxiliar = json.loads(request.POST.get('auxiliar'))
        dataFrame = json.loads(request.POST.get('dataFrame'))
        dfAuxiliar = json.loads(request.POST.get('dfAuxiliar'))
        controle = []
        primeiraLinha = True
        
        for index in range(len(dataFrame[0])):
            for aux in range(len(principal)):
                if dataFrame[0][index] == principal[aux] and aux not in controle:
                    controle.append(aux)
        
        principal = controle
        controle = []
        
        for index in range(len(dfAuxiliar[0])):
            for aux in range(len(auxiliar)):
                if dfAuxiliar[0][index] == auxiliar[aux] and aux not in controle:
                    controle.append(aux)
        
        auxiliar = controle
        
        
        if all(principal[i] <= principal[i+1] for i in range(len(principal)-1)):
            for linha in dfAuxiliar:
                controle = []
                
                if primeiraLinha:
                    primeiraLinha = False
                else:
                    for item in auxiliar:
                        controle.append(linha[item])
                        
                    dataFrame.append(controle)
            
            controle = dataFrame
        elif all(auxiliar[i] <= auxiliar[i+1] for i in range(len(auxiliar)-1)):
            for linha in dataFrame:
                controle = []
                if primeiraLinha:
                    primeiraLinha = False
                else:
                    for item in principal:
                        controle.append(linha[item])
                        
                    dfAuxiliar.append(controle)
            
            controle = dfAuxiliar
        else:
            aux = []
            
            for linha in dataFrame:
                controle = []
                
                for item in principal:
                    controle.append(linha[item])
                    
                aux.append(controle)
            
            for linha in dfAuxiliar:
                controle = []
                
                if primeiraLinha:
                    primeiraLinha = False
                else:
                    for item in auxiliar:
                        controle.append(linha[item])
                        
                    aux.append(controle)
            
            controle = aux
        
        return JsonResponse({'data': controle}, status=200)
    else:
        return JsonResponse({'error': 'Método de requisição não permitido.'}, status=405)

@csrf_exempt
def juncao_horizontal(request):
    if request.method == 'POST':
        dataFrame = json.loads(request.POST.get('dataFrame'))
        dfAuxiliar = json.loads(request.POST.get('dfAuxiliar'))
        relacao = json.loads(request.POST.get('relacao'))
        retorno = []
        posicaoPrincipal = None
        PosicaoAuxiliar = None
        
        for index in range(len(dataFrame[0])):
            if dataFrame[0][index] == relacao[0]:
                posicaoPrincipal = index
        
        for index in range(len(dfAuxiliar[0])):
            if dfAuxiliar[0][index] == relacao[1]:
                PosicaoAuxiliar = index
        
        temporario = dataFrame[0]
        temporario.extend(dfAuxiliar[0])
        retorno.append(temporario)
        
        for item in dataFrame[1:]:
            for aux in dfAuxiliar[1:]:
                if item[posicaoPrincipal] == aux[PosicaoAuxiliar]:
                    temporario = item[:]
                    temporario.extend(aux)
                    retorno.append(temporario)
        
        return JsonResponse({'data': retorno}, status=200)
    else:
        return JsonResponse({'error': 'Método de requisição não permitido.'}, status=405)


    




def verifica_colunas(valor, tabelas):
    aux = []
    
    for tabela in tabelas:
        if valor in tabelas[tabela]:
            aux.append(tabela)
    
    if len(aux) > 1:   
        return aux
    else:
        return False


'''
Limiar de Desenvolvimento
'''

def esquema(request):
    if request.method == 'POST':
        tipo = int(request.POST.get('tipo'))
        tecnologia = int(request.POST.get('tecnologia'))
        servidor = request.POST.get('servidor')
        usuario = request.POST.get('usuario')
        senha = request.POST.get('senha')
        porta = request.POST.get('porta')
        
        if porta == "":
            if tecnologia == 0: #MongoDB
                porta = "27017"
            elif tecnologia == 1: #MySQL
                porta = "3306"
            elif tecnologia == 2: #PostgreSQL
                porta = "5432"
        
        try:
            conexao = conectar(tecnologia, servidor, usuario, senha, porta)
            
            if tecnologia == 0: #MongoDB
                bancos = conexao.list_database_names()
                retorno = []
                
                bancos_default = ['admin', 'config', 'local']
                
                for banco in bancos:
                    if banco not in bancos_default:
                        retorno.append(banco)
                    
            elif tecnologia == 1: #MySQL
                cursor = conexao.cursor()
                cursor.execute("SHOW DATABASES")
                bancos = cursor.fetchall()
                retorno = []
                
                bancos_default = ['sys', 'information_schema', 'performance_schema', 'mysql']
                
                for banco in bancos:
                    if banco[0] not in bancos_default:
                        retorno.append(banco)
                        
            elif tecnologia == 2: #PostgreSQL
                cursor = conexao.cursor()
                cursor.execute("SELECT datname FROM pg_database WHERE datistemplate = false;")
                bancos = cursor.fetchall()
                retorno = []
                
                for banco in bancos:
                    if banco[0] != 'postgres':
                        retorno.append(banco[0])
                        
                cursor.close()
                conexao.close()
        except psycopg2.Error as e:
            return JsonResponse({'data': 'Erro ao conectar com a base de dados.'}, status=500)
        
        conexao = {
            'tipo': tipo,
            'tecnologia': tecnologia,
            'servidor': servidor,
            'usuario': usuario,
            'senha': senha,
            'porta': porta
        }
        
        return JsonResponse({'conexao': conexao, 'bancos': retorno}, status=200)
    else:
        return JsonResponse({'error': 'Método de requisição não permitido.'}, status=405)

def seleciona(request):
    if request.method == 'POST':
        fonte_id = request.POST.get('fonte_id')
        retorno = []
        
        fonte = get_object_or_404(Fonte, id=fonte_id)
        try:
            conexao = conectar(fonte.tecnologia, fonte.endereco, fonte.usuario, fonte.senha, fonte.porta, fonte.banco)
            tabelas = listar_tabelas(conexao, fonte.tecnologia, fonte.banco)
            
            for tabela in tabelas:
                colunas = listar_colunas(conexao, tabela, fonte.tecnologia)
                retorno.append([tabela, colunas])          
            
        except (psycopg2.Error, mysql.connector.Error, Exception) as e:
            return JsonResponse({'data': 'Erro ao conectar com a base de dados.'}, status=500)
        
        return JsonResponse({'data': retorno}, status=200)
    else:
        return JsonResponse({'error': 'Método de requisição não permitido.'}, status=405)
    
def conectar(tecnologia, servidor, usuario, senha, porta, banco=""):
    if tecnologia == 0:
        string_conexao = "mongodb://"+usuario+":"+senha+"@"+servidor+":"+porta+"/"
        conexao = pymongo.MongoClient(string_conexao, connect=False)
        if banco != "":
            conexao = conexao[banco]
    elif tecnologia == 1:
        if banco == "":
            conexao = mysql.connector.connect(user=usuario, 
                                              password=senha, 
                                              host=servidor, 
                                              port=porta)
        else:
            conexao = mysql.connector.connect(user=usuario, 
                                              password=senha, 
                                              host=servidor, 
                                              port=porta,
                                              database=banco)  
    elif tecnologia == 2:
        if banco == "":
            conexao = psycopg2.connect(host=servidor,
                               user=usuario,
                               password=senha, 
                               port=porta)
        else:
            conexao = psycopg2.connect(host=servidor,
                               user=usuario,
                               password=senha, 
                               port=porta,
                               database=banco)
    return conexao

def listar_tabelas (conexao, tecnologia, banco):
    tabelas = []
    
    if tecnologia == 0:
        tabelas = conexao.list_collection_names()            
    elif tecnologia == 1:
        cursor = conexao.cursor()
        cursor.execute("Use "+banco+";")
        cursor.execute("Show tables;")
        
        aux = cursor.fetchall()
        
        for item in aux:
            tabelas.append(item[0])
        
        cursor.close()
    elif tecnologia == 2:
        cursor = conexao.cursor()
        cursor.execute("""SELECT table_schema, table_name
                  FROM information_schema.tables
                  WHERE table_schema != 'pg_catalog'
                  AND table_schema != 'information_schema'
                  AND table_type='BASE TABLE'
                  ORDER BY table_schema, table_name""")
        
        aux = cursor.fetchall()
    
        for item in aux:
            tabelas.append(item[1])
    
        cursor.close()
    
    return tabelas

def listar_colunas(conexao, tabela, tipo):
    colunas = []
    
    if tipo == 0:
        colecao = conexao[tabela]
        documento = colecao.find_one()
        
        for key in documento:
            colunas.append(key);
        
    else:    
        cursor = conexao.cursor()
        
        cursor.execute("select column_name from information_schema.columns where table_name ='"+tabela+"'")
        aux = cursor.fetchall()
        
        for item in aux:
            colunas.append(item[0])
        
    return colunas

def alteracao_toDF(fonte, alteracoes):
    try:
        conexao = conectar(fonte.tecnologia, fonte.endereco, fonte.usuario, fonte.senha, fonte.porta, fonte.banco)
        relacoes = [None]
        
        for alteracao in alteracoes:
            if alteracao.tipo == 0:
                itens_selecionados = json.loads(alteracao.valor)     
            elif alteracao.tipo == 1:
                relacoes = json.loads(alteracao.valor)
                
        if itens_selecionados and relacoes:
            if fonte.tecnologia == 0:
                retorno = mongo_documentos(conexao, itens_selecionados, relacoes)
                return None, retorno
            else:
                colunas, query = monta_select(itens_selecionados, relacoes)
                retorno = executa_query(conexao, query)
                
            return colunas, retorno
        else:            
            return None
    except psycopg2.Error as e:
        return None
    
def mongo_documentos(conexao, tabelas, relacoes):
    campos = {}
    
    if len(tabelas) == 1:
        chave = list(tabelas.keys())[0]
        atributos = tabelas[chave]
        colecao = conexao[chave]
        
        campos = {atributo: 1 for atributo in atributos}
            
        documentos = colecao.find({},campos)
    else:
        pipeline = []
        dicionario = {}

        for relacao in relacoes:
            if relacao[0] in dicionario:
                dicionario[relacao[0]] += 1
            else:
                dicionario[relacao[0]] = 1
            
            if relacao[2] in dicionario:
                dicionario[relacao[2]] += 1
            else:
                dicionario[relacao[2]] = 1

        chave = max(dicionario, key=dicionario.get)
        colecao = conexao[chave]
        
        for relacao in relacoes:
            if relacao[0] == chave:
                pipeline.append({
                    "$lookup": {
                        "from": relacao[2],
                        "localField": relacao[1],
                        "foreignField": relacao[3],
                        "as": relacao[2]+"1"
                    }
                })
                pipeline.append(
                    {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$$ROOT",
                                    { "$arrayElemAt": ["$"+relacao[2]+"1", 0] }
                                ]
                            }
                        }
                    })
                pipeline.append(
                    { "$unset": relacao[2]+"1" }
                )

            else:
                pipeline.append({
                    "$lookup": {
                        "from": relacao[0],
                        "localField": relacao[3],
                        "foreignField": relacao[1],
                        "as": relacao[0]+"1"
                    }
                })
                pipeline.append(
                    {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$$ROOT",
                                    { "$arrayElemAt": ["$"+relacao[0]+"1", 0] }
                                ]
                            }
                        }
                    })
                pipeline.append(
                    { "$unset": relacao[0]+"1" }
                )
            
            projeto = '{"$project": {'
            
            for index, tabela in enumerate(tabelas):
                for aux, coluna in enumerate(tabelas[tabela]):
                    if index == len(tabelas)-1 and aux == len(tabelas[tabela])-1:
                        projeto += '"'+coluna+'": 1'
                    else:
                        projeto += '"'+coluna+'": 1,'
            
            projeto += "}}"
                    
            pipeline.append(json.loads(projeto)) 
            
            documentos = colecao.aggregate(pipeline)
            
    return documentos

def monta_select(tabelas, relacoes):
    colunas_sql = []
    joins_sql = []
    colunas_retorno = [coluna for colunas in tabelas.values() for coluna in colunas]
    
    if relacoes[0] != None:
        usados = [relacoes[0][0]]
    
    for tabela, colunas in tabelas.items():
        colunas_str = ', '.join([f"{tabela}.{coluna}" for coluna in colunas])
        colunas_sql.append(colunas_str)
        
    for relacao in relacoes:
        if relacao == None:
            break
        
        tabela1, coluna_relacao1, tabela2, coluna_relacao2 = relacao
        
        if tabela1 in usados:
            joins_sql.append(f" INNER JOIN {tabela2} ON {tabela1}.{coluna_relacao1} = {tabela2}.{coluna_relacao2}")
            usados.append(tabela2)
        else:
            joins_sql.append(f" INNER JOIN {tabela1} ON {tabela1}.{coluna_relacao1} = {tabela2}.{coluna_relacao2}")
            usados.append(tabela1)
            
    comando = f"SELECT {', '.join(colunas_sql)} FROM {list(tabelas.keys())[0]}"
    comando += ''.join(joins_sql) + ";"
        
    return colunas_retorno, comando

def executa_query(conexao, query):
    cursor = conexao.cursor()
    cursor.execute(query)
    
    resultado = cursor.fetchall()
    
    return resultado
