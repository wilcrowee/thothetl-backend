from django.urls import path
from . import views

urlpatterns = [
    path('esquema', views.esquema, name="esquema"),
    path('seleciona', views.seleciona, name="seleciona"),
]
    